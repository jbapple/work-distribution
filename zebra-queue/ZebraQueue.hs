module ZebraQueue (ZebraQueue, empty, divide, inject, pop, push, toList, fromList) where

{-

Divides a deque in half fairly - evens to one side, odds to the other

-}

import Data.List as List

data Party = Dem | Gop -- left and right
           deriving (Show)
                    
flop :: Party -> Party
flop Dem = Gop
flop Gop = Dem

data ZebraQueue a = Empty
                  | Leaf a
                  | Branch Party (ZebraQueue a) (ZebraQueue a) 
                -- left item is where the tail item is, Party item is
                -- where the head item is
                deriving (Show)

empty = Empty

-- O(n)
toList Empty = []
toList (Leaf x) = [x]
toList x@(Branch Dem _ _) = 
  loop [x] 
  where
    loop :: [ZebraQueue a] -> [a]
    loop xs@(Leaf _:_) = map unLeaf xs
    loop xs = loop $ splay xs []
    
    splay [] ys = reverse ys
    splay (Leaf x:xs) ys = Leaf x:splay xs ys
    splay (Branch Dem p q:xs) ys = p:splay xs (q:ys)
    splay (Branch Gop p q:xs) ys = splay (Branch Dem q p:xs) ys

    unLeaf (Leaf x) = x
toList (Branch Gop p q) = toList (Branch Dem q p)

-- O(n)
fromList :: [a] -> ZebraQueue a
fromList [] = Empty
fromList [x] = Leaf x
fromList xs = 
  go $ map (\p -> (1,Leaf p)) xs
  where 
    go :: [(Int,ZebraQueue a)] -> ZebraQueue a
    go [x] = snd x
    go xs =
      let n = length xs
          (front,back) = splitAt (pow2half n) xs
          pair xs [] = xs
          pair [] ys = ys
          pair ((i,x):xs) ((j,y):ys) = 
            (if j >= i
             then (i+j,Branch Gop y x)
             else (i+j,Branch Dem x y)):(pair xs ys)
          pow2half x = help x 1
            where
              help x y =
                if y * 2 < x
                then help x (y*2)
                else y
      in go $ pair front back
      

divide :: ZebraQueue a -> (ZebraQueue a, ZebraQueue a)
divide Empty = (Empty,Empty)
divide (Leaf a) = (Empty, Leaf a)
divide (Branch dir left rite) =
  case dir of
    Dem -> (left, rite)
    Gop -> (rite, left)
    
push :: a -> ZebraQueue a -> ZebraQueue a
push x Empty = Leaf x
push x (Leaf y) = Branch Gop (Leaf y) (Leaf x)
push x (Branch dir left rite) =
  case dir of 
    Dem -> Branch Gop left (push x rite)
    Gop -> Branch Dem (push x left) rite
    
inject :: ZebraQueue a -> a -> ZebraQueue a
inject Empty x = Leaf x
inject (Leaf y) x = Branch Gop (Leaf x) (Leaf y)
inject (Branch dir left rite) x =
  Branch (flop dir) (inject rite x) left
  
pop :: ZebraQueue a -> Maybe (a, ZebraQueue a)
pop Empty = Nothing
pop (Leaf x) = Just (x,Empty)
pop (Branch Dem left rite) =
  case pop left of
    Nothing -> Nothing -- TODO: this is impossible
    Just (x,Empty) -> Just (x,rite)
    Just (x,left) -> Just (x,Branch Gop left rite)
pop (Branch Gop left rite) =
  case pop rite of
    Nothing -> Nothing -- TODO: this is impossible
    Just (x,Empty) -> Just (x,left)
    Just (x,rite) -> Just (x,Branch Dem left rite)
    
eject :: ZebraQueue a -> Maybe (ZebraQueue a, a)
eject Empty = Nothing
eject (Leaf x) = Just (Empty, x)
eject (Branch dir left rite) =
  case eject left of
    Nothing -> Nothing -- TODO: this is impossible
    Just (Empty,x) -> Just (rite,x)
    Just (left,x) -> Just (Branch (flop dir) rite left,x)